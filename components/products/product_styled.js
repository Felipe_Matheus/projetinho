import styled from "styled-components";

export const Container = styled.div`
  display: flex;
  width: 100vh;
  height: 100vw;
  background-color: blue;
`;

export const Title = styled.h1`
  font-size: 20px;
  color: #ccc;
`;

export const ProductImage = styled.img`
  width: 35px;
  height: 35px;
`;

export const Description = styled.p`
  font-size: 15px;
`;

export const Price = styled.h3`
  font-size: 18px;
`;

export const PriceWeight = styled.h3`
  font-size: 20px;
`;
