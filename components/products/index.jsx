import {
  Container,
  Title,
  ProductImage,
  Description,
  PriceWeight,
} from "./product_styled";

const Products = ({ product_list }) => {
  product_list.map(({ name, description, price, img, Weight, Price }, key) => (
    <Container key={key}>
      <Title>name:{name}</Title>
      <ProductImage src={img} alt="" />
      <Description>description:{description}</Description>
      <Price>price:{price}</Price>
      <PriceWeight>weight:{Weight}</PriceWeight>
    </Container>
  ));
};

export default Products;
