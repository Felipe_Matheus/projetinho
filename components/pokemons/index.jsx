import axios from "axios";

const GetPokemons = ({ setProducts }) => {
  axios.get("pokemon?limit=20&offset=0").then(({ data }) => {
    setProducts(data);
  });
};

export default GetPokemons;
